#ifndef KNITTINGGUI_H
#define KNITTINGGUI_H

#include <QMainWindow>
#include "ImageAnalysis.h"

namespace Ui {
class KnittingGUI;
}

class KnittingGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit KnittingGUI(QWidget *parent = 0);
    ~KnittingGUI();

private:
    ImageAnalysis* analysis;
private slots:
    void on_pushButton_clicked();

    void on_openFile_Dialog_clicked();

    void on_saveFile_Dialog_clicked();

    void on_actionAbout_2_triggered();

    void on_actionExit_triggered();

    void onAnalysisError(QString msg);
    void onAnalysisComplete(QString msg);

    void on_actionSettings_2_triggered();

private:
    Ui::KnittingGUI *ui;
};

#endif // KNITTINGGUI_H
