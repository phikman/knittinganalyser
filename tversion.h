#ifndef TVERSION_H
#define TVERSION_H

#include <QObject>

class tVersion : public QObject
{
    Q_OBJECT
public:
    explicit tVersion(QObject *parent = 0);
    static QString getVersion();
    static QString getDeveloper();
    static QString getGitRepo();

private:
signals:

public slots:
};

#endif // TVERSION_H
