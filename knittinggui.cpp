#include "knittinggui.h"
#include "ui_knittinggui.h"

#include "ImageAnalysis.h"
#include "tversion.h"
#include "SettingsBlock.h"
#include "defaultsettings.h"

#include "settingsdialog.h"

#include <QFileDialog>
#include <QMessageBox>

KnittingGUI::KnittingGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KnittingGUI)
{
    Q_INIT_RESOURCE(images);

    ui->setupUi(this);
    ui->columns->setValidator(new QIntValidator(0, 500, this));
    ui->rows->setValidator(new QIntValidator(0, 500, this));

    analysis = NULL;
    this->setWindowIcon(QIcon(":/knitting_3.png"));

    ui->columns->setText(QString::number(COLUMNS_COUNT__DEFAULT));
    ui->rows->setText(QString::number(ROWS_COUNT__DEFAULT));
}

KnittingGUI::~KnittingGUI()
{
    if(analysis)
    {
        delete analysis;
    }

    delete ui;
}

#include <QDebug>

void KnittingGUI::on_pushButton_clicked()
{
    int num_cols =  ui->columns->text().toInt();
    int num_rows = ui->rows->text().toInt();

    SettingsBlock* app_settings = SettingsBlock::instance();

    double row_height = app_settings->getValue_Double("excel/cell_height", EXCEL_CELL_HEIGHT__DEFAULT);
    double col_width = app_settings->getValue_Double("excel/cell_width", EXCEL_CELL_WIDTH__DEFAULT);
    ColorAlgorithm algo = (ColorAlgorithm)app_settings->getValue_Int("analysis/algorithm", ALGORITHM__DEFAULT);


    if(num_rows <= 0 || num_cols <= 0 || row_height <= 0 || col_width <= 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Please set options");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
        msgBox.exec();
        ui->msg->setText("Please set options");
        return;
    }

    if(ui->outputFile->text().isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Please select save file");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
        msgBox.exec();
        ui->msg->setText("Please select save file");
        return;
    }

    if(analysis)
    {
        QMessageBox msgBox;
        msgBox.setText("Please wait...\r\nAnd when try again in a moment");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
        msgBox.exec();
        ui->msg->setText("Please wait...And when try again in a moment");
        return;
    }

    analysis = new ImageAnalysis();
    connect(analysis, SIGNAL(Completed(QString)), this, SLOT(onAnalysisComplete(QString)));
    connect(analysis, SIGNAL(Error(QString)), this, SLOT(onAnalysisError(QString)));

    ui->msg->setText("Thinking...");

    analysis->AnalyiseImage(ui->inputFile->text(),num_cols,num_rows,ui->outputFile->text(), row_height, col_width, algo);
}

void KnittingGUI::on_openFile_Dialog_clicked()
{
    SettingsBlock* app_settings = SettingsBlock::instance();
    QString openDir = app_settings->getValue_String("application/open_directory", OPEN_DIRECTORY__DEFAULT);
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open Image"), openDir, tr("Image Files (*.png *.jpg *.bmp *.gif)"));

    ui->inputFile->setText(file_name);

    QImage displayImg;
    displayImg.load(ui->inputFile->text());

    ui->inputImg->setPixmap(QPixmap::fromImage(displayImg.scaled(ui->inputImg->width(), ui->inputImg->height(), Qt::AspectRatioMode::KeepAspectRatio)));
}

void KnittingGUI::on_saveFile_Dialog_clicked()
{
    ui->outputFile->setText(QFileDialog::getSaveFileName(this, tr("Save File"), "C://", tr("Excel Spreadsheets (*.xlsx)")));
}

void KnittingGUI::on_actionAbout_2_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Version: " + tVersion::getVersion() + "\r\nDeveloped by " + tVersion::getDeveloper() + "\r\nThanks to Rosie for her ideas and help :)\r\nSource avalaible at `"+tVersion::getGitRepo()+"`");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
    msgBox.exec();
}

void KnittingGUI::on_actionExit_triggered()
{
    if(analysis)
    {
        analysis->wait(10000);
        delete analysis;
        analysis = NULL;
    }
    exit(0);
}

void KnittingGUI::onAnalysisError(QString msg)
{
    ui->msg->setText(msg);

    QMessageBox msgBox;
    msgBox.setText("Error: " + msg);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
    msgBox.exec();

    if(analysis)
    {
        try{
            disconnect(analysis, SIGNAL(Completed(QString)), this, SLOT(onAnalysisComplete(QString)));
            disconnect(analysis, SIGNAL(Error(QString)), this, SLOT(onAnalysisError(QString)));

            delete analysis;
            analysis = NULL;

        }
        catch(...)
        {
            qDebug() << "Failed to delete anaysis";
        }
    }
}

void KnittingGUI::onAnalysisComplete(QString msg)
{
    ui->msg->setText(msg);

    QMessageBox msgBox;
    msgBox.setText("Completed!\r\n" + msg);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
    msgBox.exec();

    if(analysis)
    {
        try{
            disconnect(analysis, SIGNAL(Completed(QString)), this, SLOT(onAnalysisComplete(QString)));
            disconnect(analysis, SIGNAL(Error(QString)), this, SLOT(onAnalysisError(QString)));

            delete analysis;
            analysis = NULL;

        }
        catch(...)
        {
            qDebug() << "Failed to delete anaysis";
        }
    }
}

void KnittingGUI::on_actionSettings_2_triggered()
{
    SettingsDialog* diag = new SettingsDialog(this);

    diag->show();
}
