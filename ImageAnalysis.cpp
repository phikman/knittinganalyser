#include "ImageAnalysis.h"

#include "xlsxdocument.h"
#include "xlsxformat.h"

#include <QDebug>

#define ADD_COLOUR_PALLET_off
#define USE_AVERAGE_COLOR

ImageAnalysis::ImageAnalysis(QObject *parent) : QThread(parent)
{
}

ImageAnalysis::~ImageAnalysis()
{
    mutex.lock();
    condition.wakeOne();
    mutex.unlock();

    wait();
}

bool colour_lessThan(const QColor &v1, const QColor &v2)
{
    return v1.rgba() < v2.rgba();
}

//Gaussian blur from center image for color analysis.
// Look at sharpening.

void ImageAnalysis::run()
{
    QImage image;
    int cell_width = -1;
    int cell_height = -1;

    mutex.lock();
    QString file_name = this->file_name;
    int x_cells = this->x_cells;
    int y_cells = this->y_cells;
    QString out_file = this->out_file;
    double row_height = this->row_height;
    double column_height = this->column_height;
    mutex.unlock();

    QXlsx::Document xlsx;

    QList<QColor> colour_pallet;

    try
    {
        if(!image.load(file_name))
        {
            qDebug() << "Failed to load image: " + file_name;
            emit Error("Failed to load: "+ file_name);
            return;
        }
    }
    catch(...)
    {
        qDebug() << "Exception loading image";
        emit Error("Failed to load: "+ file_name);
        return;
    }

    cell_width = image.width()/x_cells;
    cell_height = image.height()/y_cells;

    qDebug() << "cell_width = " << cell_width;
    qDebug() << "cell_height = " << cell_height;

    qDebug() << "Using algorithm " << this->algorithm;

    switch(this->algorithm)
    {
    case ColorType__CenterPixel:
    {
        int i, j;
        for(i = (cell_width/2); i < image.width(); i+=cell_width)
        {
            for(j = (cell_height/2); j < image.height();j += cell_height)
            {
                int row = (j/cell_height)+1;
                int col = (i/cell_width)+1;

                QColor segment_color(image.pixel(i,j));

                QXlsx::Format cell_format;
                cell_format.setPatternBackgroundColor(segment_color);
                cell_format.setBorderStyle(QXlsx::Format::BorderStyle::BorderThin);
                xlsx.write(row,col, "", cell_format);

                if(!colour_pallet.contains(segment_color))
                {
                    colour_pallet.append(segment_color);
                }
            }
        }
    }
        break;

    case ColorType__Average:

    {
        int i, j;
        for(i = 0; i < image.width(); i+=cell_width)
        {
            for(j = 0; j < image.height();j += cell_height)
            {
                int row = (j/cell_height)+1;
                int col = (i/cell_width)+1;

                long unsigned int avg_r = 0;
                long unsigned int avg_g = 0;
                long unsigned int avg_b = 0;

                int counter = 0;

                //Logic bug!!
                //Check pixel index
                for(int k = i; k <= (i+cell_width); k++)
                {
                    for(int l = j; l <= (j+cell_height); l++)
                    {
                        avg_r = qRed(image.pixel(k,l));
                        avg_g = qGreen(image.pixel(k,l));
                        avg_b = qBlue(image.pixel(k,l));

                        counter++;
                    }
                }

                QColor segment_color(qRgba((avg_r/counter), (avg_g/counter), (avg_b/counter), 0));

                QXlsx::Format cell_format;
                cell_format.setPatternBackgroundColor(segment_color);
                cell_format.setBorderStyle(QXlsx::Format::BorderStyle::BorderThin);
                xlsx.write(row,col, "", cell_format);

                if(!colour_pallet.contains(segment_color))
                {
                    colour_pallet.append(segment_color);
                }
            }
        }
    }
        break;
    }

#ifdef ADD_COLOUR_PALLET
    qSort(colour_pallet.begin(), colour_pallet.end(), colour_lessThan);

    for(int k = 0; k < colour_pallet.size(); k++)
    {
        QXlsx::Format cell_colour;
        cell_colour.setPatternBackgroundColor(colour_pallet.at(k));
        cell_colour.setBorderStyle(QXlsx::Format::BorderStyle::BorderThin);
        xlsx.write(k + 1,(i/cell_width)+3, "", cell_colour);
    }
#endif
    xlsx.setColumnWidth(1, (image.width()/cell_width)+100, column_height);
    xlsx.setRowHeight(1, (image.height()/cell_height)+100, row_height);

    try
    {

        if(!xlsx.saveAs(out_file))
        {
            qDebug() << "Failed to save Excel sheet: " + out_file;
            emit Error("Failed to save: "+ out_file);
            return;
        }
    }
    catch(...)
    {
        qDebug() << "Exception saving image";
        emit Error("Failed to save: "+ out_file);
        return;
    }


    emit Completed("Saved " + file_name + " to " + out_file + " successfully");
}

void ImageAnalysis::AnalyiseImage(QString file_name, int x_cells, int y_cells, QString out_file, double row_height, double column_height, ColorAlgorithm algorithm)
{
    QMutexLocker locker(&mutex);

    this->file_name = file_name;
    this->x_cells = x_cells;
    this->y_cells = y_cells;
    this->out_file= out_file;
    this->row_height = row_height;
    this->column_height = column_height;
    this->algorithm = algorithm;

    if (!isRunning())
    {
        start(LowPriority);
    } else
    {
        condition.wakeOne();
    }
}

