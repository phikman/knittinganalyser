#-------------------------------------------------
#
# Project created by QtCreator 2016-05-22T22:10:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KnittingAnalyser_GUI
TEMPLATE = app


SOURCES += main.cpp\
        knittinggui.cpp \
    ImageAnalysis.cpp \
    tversion.cpp \
    settingsdialog.cpp \
    SettingsBlock.cpp

HEADERS  += knittinggui.h \
    ImageAnalysis.h \
    tversion.h \
    version_custom.h \
    settingsdialog.h \
    SettingsBlock.h \
    defaultsettings.h

FORMS    += knittinggui.ui \
    settingsdialog.ui

include(3rdParty/QtXlsxWriter/src/xlsx/qtxlsx.pri)

DISTFILES +=

RESOURCES += \
    images.qrc
