#include "knittinggui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Phik Inc.");
    QCoreApplication::setApplicationName("Knitting Analyser");

    QApplication a(argc, argv);
    KnittingGUI w;
    w.show();

    return a.exec();
}
