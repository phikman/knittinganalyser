#include "SettingsBlock.h"

#include <QDebug>

SettingsBlock* SettingsBlock::ptr_local_instance = NULL;

SettingsBlock::SettingsBlock(QObject *parent) : QObject(parent)
{
    settings = new QSettings(0);

    if(!settings)
    {
        qDebug() << "Couldn't allocate settings";
    }
    else
    {
        qDebug() << "Save location: " << settings->fileName();
        settings->sync();
    }
}

SettingsBlock::~SettingsBlock()
{
    if(settings)
    {
        try
        {
            settings->sync();
            delete settings;
            settings = NULL;
        }
        catch(...)
        {

        }
    }
}

int SettingsBlock::getValue_Int(QString key, int default_value)
{
    int retVal = default_value;
    mutex.lockForRead();

    if(settings && settings->contains(key))
    {
        bool isOk = false;
        retVal = settings->value(key, default_value).toInt(&isOk);
        retVal = (isOk ? retVal : default_value);
    }

    mutex.unlock();
    return retVal;
}

double SettingsBlock::getValue_Double(QString key, double default_value)
{
    double retVal = default_value;

    mutex.lockForRead();

    if(settings && settings->contains(key))
    {
        bool isOk = false;
        retVal = settings->value(key, default_value).toDouble(&isOk);
        retVal = (isOk ? retVal : default_value);
    }

    mutex.unlock();

    return retVal;
}

QString SettingsBlock::getValue_String(QString key, QString default_value)
{
    QString retVal = default_value;

    mutex.lockForRead();

    if(settings && settings->contains(key))
    {
        retVal = settings->value(key, default_value).toString();
    }

    mutex.unlock();

    return retVal;
}

bool SettingsBlock::setValue_Int(QString key, int value)
{
    bool retVal = false;
    mutex.lockForWrite();

    if(settings)
    {
        settings->setValue(key, QVariant(value));
        settings->sync();

        retVal = true;
    }
    mutex.unlock();
    return retVal;
}

bool SettingsBlock::setValue_Double(QString key, double value)
{
    bool retVal = false;
    mutex.lockForWrite();

    if(settings)
    {
        settings->setValue(key, QVariant(value));
        settings->sync();

        retVal = true;
    }
    mutex.unlock();
    return retVal;
}

bool SettingsBlock::setValue_String(QString key, QString value)
{
    bool retVal = false;
    mutex.lockForWrite();

    if(settings)
    {
        settings->setValue(key, QVariant(value));
        settings->sync();

        retVal = true;
    }
    mutex.unlock();
    return retVal;
}
