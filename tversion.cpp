#include "tversion.h"
#include "version_custom.h"


tVersion::tVersion(QObject *parent) : QObject(parent)
{
}

QString tVersion::getVersion()
{
    return VERSION_STR;
}

QString tVersion::getDeveloper()
{
    return QString(DEVELOPER_STR) + " `" + QString(DEVELOPER_EMAIL_STR) + "`";
}

QString tVersion::getGitRepo()
{
    return QString(GIT_REPO);
}
