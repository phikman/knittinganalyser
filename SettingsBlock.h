#ifndef SETTINGSBLOCK_H
#define SETTINGSBLOCK_H

#include <QObject>
#include <QSettings>

#include <QReadWriteLock>

class SettingsBlock : public QObject
{
    Q_OBJECT
public:
    explicit SettingsBlock(QObject *parent = 0);
    ~SettingsBlock();

    int getValue_Int(QString key, int default_value = -1);
    double getValue_Double(QString key, double default_value = NAN);
    QString getValue_String(QString key, QString default_value = QString());

    bool setValue_Int(QString key, int value);
    bool setValue_Double(QString key, double value);
    bool setValue_String(QString key, QString value);

    static SettingsBlock* instance()
    {
        if(!ptr_local_instance)
        {
            ptr_local_instance = new SettingsBlock();
        }

        return ptr_local_instance;
    }
    static bool isConfigured(){return (ptr_local_instance?true:false);}

private:
    QSettings* settings;
    QReadWriteLock mutex;

    static SettingsBlock* ptr_local_instance;

signals:

public slots:
};

#endif // SETTINGSBLOCK_H
