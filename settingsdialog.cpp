#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QDebug>
#include <QMessageBox>

#include "SettingsBlock.h"
#include "defaultsettings.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    SettingsBlock *app_settings = SettingsBlock::instance();

    ui->setupUi(this);

    ui->colWidth->setValidator(new QDoubleValidator(0, 100, 2, this));
    ui->rowHeight->setValidator(new QDoubleValidator(0,100, 2, this));

    ui->colWidth->setText(QString::number(app_settings->getValue_Double("excel/cell_width", EXCEL_CELL_WIDTH__DEFAULT)));
    ui->rowHeight->setText(QString::number(app_settings->getValue_Double("excel/cell_height", EXCEL_CELL_HEIGHT__DEFAULT)));
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}


void SettingsDialog::on_btnSave_clicked()
{
    SettingsBlock *app_settings = SettingsBlock::instance();

    app_settings->setValue_Double("excel/cell_width", ui->colWidth->text().toDouble());
    app_settings->setValue_Double("excel/cell_height", ui->rowHeight->text().toDouble());

    this->close();
}

void SettingsDialog::on_btnDefaults_clicked()
{
    SettingsBlock *app_settings = SettingsBlock::instance();

    qDebug() << "Resetting to defaults!!";

    app_settings->setValue_Double("excel/cell_width", EXCEL_CELL_WIDTH__DEFAULT);
    app_settings->setValue_Double("excel/cell_height", EXCEL_CELL_HEIGHT__DEFAULT);
    app_settings->setValue_Int("analysis/algorithm", ALGORITHM__DEFAULT);

    ui->colWidth->setText(QString::number(app_settings->getValue_Double("excel/cell_width", EXCEL_CELL_WIDTH__DEFAULT)));
    ui->rowHeight->setText(QString::number(app_settings->getValue_Double("excel/cell_height", EXCEL_CELL_HEIGHT__DEFAULT)));

    QMessageBox msgBox;
    msgBox.setText("Default settings restored!");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setWindowIcon(QIcon(":/knitting_3.png"));
    msgBox.exec();
}

void SettingsDialog::on_btnCancel_clicked()
{
    this->close();
}
