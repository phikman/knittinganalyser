#ifndef VERSION_CUSTOM_H
#define VERSION_CUSTOM_H

#define VERSION_STR         "0.2"
#define DEVELOPER_STR       "Phill Ogden"
#define DEVELOPER_EMAIL_STR "phill.ogden717@gmail.com"
#define GIT_REPO            "bitbucket.org/phikman/knittinganalyser.git"

#endif // VERSION_CUSTOM_H
