#ifndef IMAGEANALYSIS_H
#define IMAGEANALYSIS_H

#include <QObject>
#include <QImage>
#include <QMutex>
#include <QSize>
#include <QThread>
#include <QWaitCondition>

typedef enum {
    ColorType__CenterPixel,
    ColorType__Average,
}ColorAlgorithm;

class ImageAnalysis : public QThread
{
    Q_OBJECT
public:
    explicit ImageAnalysis(QObject *parent = 0);
    ~ImageAnalysis();
    void AnalyiseImage(QString file_name, int x_cells, int y_cells, QString out_file, double row_height, double column_height, ColorAlgorithm algorithm);

protected:
    void run() Q_DECL_OVERRIDE;

private:
    QMutex mutex;
    QWaitCondition condition;

    QString file_name;
    int x_cells;
    int y_cells;
    QString out_file;
    double row_height;
    double column_height;
    ColorAlgorithm algorithm;

signals:
    void Error(QString msg);
    void Completed(QString msg);
public slots:
};

#endif // IMAGEANALYSIS_H
